pipeline {
  environment {
    registry = "https://devcognira.azurecr.io"
    registryCredential = "docker-login"
  }

  agent any

  stages {
    stage('Pre-Build'){
      steps{
        sh 'bash script/create-promo-network.sh'
      }
    }
    stage('Build') {
      steps {     
        sh '/usr/local/bin/docker-compose -f api/build.yml run --rm sbt sbt pack'
        sh '/usr/local/bin/docker-compose build security-api'
        sh '/usr/local/bin/docker-compose -f acceptance-test.yml build'
      }
    }
    stage('Run') {
      steps {
        sh 'if [ "`docker ps -qf \'name=cassandra\'`" == "" ]; then /usr/local/bin/docker-compose -f cassandra.yml up -d; else echo "Cassandra container already up and running"; fi'  
        sh '/usr/local/bin/docker-compose up -d'  
      }         
    }
    stage('Run Schema Manager') {
       steps {
         sh '/usr/local/bin/docker-compose -f schema-manager.yml run -v $(pwd)/schema-manager/tables.list:/root/tables.list --rm schema-manager -k security_service -tf /root/tables.list'
       }
    }
    stage('Test') {
      steps {
        sh '/usr/local/bin/docker-compose -f api/build.yml run --rm sbt sbt test'
        sh '/usr/local/bin/docker-compose -f acceptance-test.yml up'
      }   
    }    
  }

  post {
    success {
      script{
        docker.withRegistry(registry, registryCredential){    
          echo 'Pushing Promo Planning Service API image'
          sh 'docker push devcognira.azurecr.io/security-service/security_api' 
        }
      }
    }

    always {
      sh '/usr/local/bin/docker-compose -f cassandra.yml down -v'
      sh '/usr/local/bin/docker-compose down -v'
      sh '/usr/local/bin/docker-compose -f acceptance-test.yml down -v'
    }
  }  
}