# Cognira Security Service #

This repository holds all the backend components for the Cognira security service.

## Prerequisites

Make sure you have Docker installed and that you log in to Cognira Azure Container registry
```
docker login --username devCognira devcognira.azurecr.io
```

Reach out to DevOps team to get the password

## Setup Environment

In order to run, most of the containers needs to have the right credentials to access a Cassandra database, whether locally or on the cloud (Azure Cosmos DB).

The following file also allows us to specify whether we want to run in "dev" or "prod" modes thanks to the parameter `MODE` below.

Please update the file `config/env.conf` with the right credentials before runtime to be able to access the database.

---
The file `config/env.conf` consists of the following:

* `MODE`="dev" to use cassandra as a docker container. "prod" to use Azure Cosmos DB
* `DB_HOST`=The hostname of the Cassandra database
* `DB_PORT`=The port to use to connect to the Cassandra database
* `DB_USER`=The database user to connect with
* `DB_PASS`=The password to use to connect to the Cassandra database. Make sure the password is NOT QUOTED!
* `SSL_VERSION`=The SSL version to use (TLSv1_2)
* `SSL_VALIDATE=false`
* `SSL_CERTFILE`=Path to the certificate file. This is where the cert file should be in the docker container on runtime.
---

## Dev vs Prod

When working on "prod" mode (`MODE=prod`), you need to use an SSL certificate. To enable that, you need to add 2 lines to all the services in the docker-compose.yml and acceptance-test.yml files:

```
volumes:
    - /path/to/certificate/local:/path/to/certificate/container
```

The `/path/to/certificate/container` is simply the `SSL_CERTFILE` path defined in `env.conf`.

## Build

### One Script Build

You can build and serve the whole application with all of its three parts (or any subset of them) using the following scripts:

1. `build.sh`: Builds the application components.
2. `serve.sh`: Serves the application components.
3. `deploy.sh`: Builds and serves the application components.

These scripts have an identical interface, each one of them takes three mandatory arguments that set the paths of the different codebases, and three optional flag arguments to dictate which parts to build and which parts to ignore.

Each script exits when done or if any of the invoked commands fails, here are a few usage examples using the `deploy.sh` script:
```sh
# Build and serve whole application
./script/deploy.sh --security-path path/to/security --promo-path path/to/promo --ui-path path/to/ui

# Build and serve without Security service
./script/deploy.sh --security-path path/to/security --promo-path path/to/promo --ui-path path/to/ui --no-security

# Build and serve without Promo service
./script/deploy.sh --security-path path/to/security --promo-path path/to/promo --ui-path path/to/ui --no-promo

# Build and serve without UI
./script/deploy.sh --security-path path/to/security --promo-path path/to/promo --ui-path path/to/ui --no-ui
```

You can mix the optional parameters to your liking, for additional information, please run the following:
```
./script/deploy.sh --help
```

### Step By Step Build

To build the Security service with step-by-step commands, please do the following:

#### Create Network

In order for all the different containers to know each other, they need to be on the same network.

You need to switch to the Cognira Promo Planning repository and run the following command:
```
./script/create-promo-network.sh
```

This network will be used within the docker-compose file.

#### Cassandra Database

When working on "dev" mode (`MODE=dev`), to spin up a Cassandra docker container, you need to switch to the Cognira Promo Planning service repository and run the following command:

```
docker-compose -f cassandra.yml up -d
```

#### Security Service API

##### 1. Package the application code

Create a packaged runnable version of the application

```
docker-compose -f api/build.yml run --rm sbt sbt pack
```

##### 2. Run the Schema Manager

Use the Cognira Schema Manager service to deploy the schema of the security using the following command:
```
docker-compose -f schema-manager.yml run\
    -v $(pwd)/schema-manager/tables.list:/root/tables.list\
    --rm schema-manager\
        -k security_service\
        -tf /root/tables.list
```

Documentation on the Schema Manager service can be found here:
https://bitbucket.org/cognira/schema-manager/src/master/

##### 2. Run the security service API

To run the security API

```
docker-compose up --build -d
```

If you're using `docker-machine` then the API will be available at `<docker-machine ip>:9001` otherwise it should be `localhost:9001`

## Tests

### Unit tests

To unit test the Promo Planning API simply run the following command:

```
docker-compose -f api/build.yml run --rm sbt sbt test
```

### Acceptance tests

In order to run the acceptance tests around the security API, make sure the API is up and running then do:
```
docker-compose -f acceptance-test.yml up --build
```
irtest 15OCT2019 01
irtest 15OCT2019 02

irtest gitlab.heb.com to azure devops 614
1.  gitlab.heb.com to azure devops and gitlab.com 24OCT2018 912
2.  gitlab.heb.com to azure devops and gitlab.com 24OCT2029 931
3.  gitlab.heb.com to azure devops and gitlab.com 24OCT2019 332
4.  gitlab.heb.com to azure devops and gitlab.com 24OCT2019 339
5.  gitlab.heb.com to azure devops and gitlab.com 25OCT2019 1122
6.  gitlab.heb.com to azure devops and gitlab.com 28OCT2019 1037
7.  gitlab.heb.com to azure devops and gitlab.com jenkins.heb trigger for test_ext_webhks 28OCT2019 218
8.  gitlab.heb.com to azure devops and gitlab.com jenkins.heb trigger for test_ext_webhks 28OCT2019 228
9.  gitlab.heb.com to azure devops and gitlab.com jenkins.heb trigger for test_ext_webhks 28OCT2019 237
10.  gitlab.heb.com to azure devops and gitlab.com jenkins.heb trigger for test_ext_webhks 28OCT2019 808
11.  gitlab.heb.com to azure devops and gitlab.com jenkins.heb trigger for test_ext_webhks 28OCT2019 821
12.  gitlab.heb.com to azure devops and gitlab.com jenkins.heb trigger for test_ext_webhks 28OCT2019 841
13.  30OCT2019 01
14.  05NOV2019 01
15.  05NOV2019 02
16.  06nov2019 01
17.  08nov2019 01
18.  09nov2019 01
19.  11n0v2019 01