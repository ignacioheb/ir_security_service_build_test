package com.cognira.routes

import akka.http.scaladsl.model.{ContentTypes, StatusCodes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.util.ByteString
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{Matchers, WordSpec}


trait PingRouteSpec extends WordSpec
  with Matchers
  with ScalatestRouteTest
  with ScalaFutures {

  "PingRoutes" should {

    "Ping security service" in {
      val pingEntity = ByteString(s"""{"timeIn": "", "timeOut": ""}""")

      val request = Post("/ping").withEntity(ContentTypes.`application/json`, pingEntity)

      request ~> Route.seal(PingRoute.route) ~> check {
        status shouldBe StatusCodes.OK
      }
    }
  }

}
