package com.cognira.routes

import org.cassandraunit.utils.EmbeddedCassandraServerHelper
import org.cassandraunit.CQLDataLoader
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet
import org.scalatest.BeforeAndAfterAll


class CassandraSuite extends UserRouteSpec with TenantRouteSpec with PingRouteSpec with BeforeAndAfterAll {

  override def beforeAll(): Unit = {
    EmbeddedCassandraServerHelper.startEmbeddedCassandra("cassandra.yaml",5600000L)
    val cluster = EmbeddedCassandraServerHelper.getCluster
    val session = cluster.newSession()

    val dataLoader = new CQLDataLoader(session)
    dataLoader.load(new ClassPathCQLDataSet("init-test-feed.cql"))

    session.execute("USE security_service")
    session.close()
    cluster.close()
  }
}