package com.cognira.routes

import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.{ContentTypes, MessageEntity, StatusCodes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{Matchers, WordSpec}
import pdi.jwt.JwtAlgorithm
import akka.http.scaladsl.testkit.RouteTestTimeout
import scala.concurrent.duration._
import akka.actor.ActorSystem
import pdi.jwt.JwtAlgorithm


import com.cognira.models.{JwtToken, User, UserLogin}
import com.cognira.utils.JWTutils._


trait UserRouteSpec extends WordSpec
  with Matchers
  with ScalatestRouteTest
  with LoginMarshaller
  with ScalaFutures {

  implicit def default(implicit system: ActorSystem) = RouteTestTimeout(20.second)

  private def commonChecks = {
    status shouldBe StatusCodes.OK
    contentType shouldBe ContentTypes.`application/json`
  }

  private val userRoute = Route.seal(UserRoutes.routes)

  "UserRoutes" should {

    "Add user to database" in {
      val user_to_add: User = User(0, 1, "user", "User", "user@gmail.com", "dummypass", "language")
      val user = Marshal(user_to_add).to[MessageEntity].futureValue
      val request = Post("/user").withEntity(user)

      request ~> userRoute ~> check {
        status shouldBe StatusCodes.OK
        responseAs[String] shouldEqual "User added successfully"
      }
    }

    "Update user in database" in {
      val user_to_update: User = User(0, 1, "user", "User", "user@gmail.com", "newpassword", "language")
      val user = Marshal(user_to_update).to[MessageEntity].futureValue
      val request = Patch("/user/1").withEntity(user)

      request ~> userRoute ~> check {
        status shouldBe StatusCodes.OK
        responseAs[String] shouldEqual "User updated successfully"
      }
    }

    "Delete user from database" in {
      val user_to_delete: User = User(0, 1, "user", "User", "user@gmail.com", "newpassword", "language")
      val user = Marshal(user_to_delete).to[MessageEntity].futureValue
      val request = Delete("/user/1").withEntity(user)

      request ~> userRoute ~> check {
        status shouldBe StatusCodes.OK
        responseAs[String] shouldEqual "User deleted successfully"
      }
    }

    "Get user from database" in {
      val request = Get("/user/0")

      request ~> userRoute ~> check {
        commonChecks
        responseAs[List[User]] should have length 1
      }
    }
  }

  "LoginRoutes" should {

    "Fail login with wrong password" in {
      val credentials: UserLogin = UserLogin("user", "wrongpassword")
      val user_login = Marshal(credentials).to[MessageEntity].futureValue
      val request = Post("/login").withEntity(user_login)

      request ~> userRoute ~> check {
        response.status shouldEqual StatusCodes.Unauthorized
      }
    }

    "Fail login with wrong username" in {
      val credentials: UserLogin = UserLogin("wronguser", "correctpassword")
      val user_login = Marshal(credentials).to[MessageEntity].futureValue
      val request = Post("/login").withEntity(user_login)

      request ~> userRoute ~> check {
        response.status shouldEqual StatusCodes.Unauthorized
      }
    }

    "Pass login with correct credentials" in {
      val credentials: UserLogin = UserLogin("user", "correctpassword")
      val user_login = Marshal(credentials).to[MessageEntity].futureValue
      val request = Post("/login").withEntity(user_login)

      request ~> userRoute ~> check {
        status shouldBe StatusCodes.OK
        responseAs[String] shouldEqual "[{\"language\":\"english\",\"username\":\"user\"}]"
      }
    }
  }

  "CheckAuthRoutes" should {

    val username: String = "user"
    val role: String = "User"
    val user_id: String = "0"

    "Grant authorization with valid JWT" in {
      val jwt: String = GenerateJWT(username, user_id, role, secretKey, algorithm, tokenExpiryPeriodInMinutes)
      val JWTToken = Marshal(JwtToken(jwt)).to[MessageEntity].futureValue
      val request = Post("/checkAuth").withEntity(JWTToken)
      request ~> userRoute ~> check {
        status shouldBe StatusCodes.OK
      }
    }

    "Fail authorization with expired JWT" in {
      val jwt: String = GenerateJWT(username, user_id, role, secretKey, algorithm, 0)
      val JWTToken = Marshal(JwtToken(jwt)).to[MessageEntity].futureValue
      val request = Post("/checkAuth").withEntity(JWTToken)
      request ~> userRoute ~> check {
        response.status shouldEqual StatusCodes.Unauthorized
      }
    }

    "Fail authorization with JWT encoded using a wrong secret key" in {
      val wrong_secret: String = "wrong_secret_key"
      val jwt: String = GenerateJWT(username, user_id, role, wrong_secret, algorithm, tokenExpiryPeriodInMinutes)
      val JWTToken = Marshal(JwtToken(jwt)).to[MessageEntity].futureValue
      val request = Post("/checkAuth").withEntity(JWTToken)
      request ~> userRoute ~> check {
        response.status shouldEqual StatusCodes.Unauthorized
      }
    }

    "Fail authorization with JWT encoded using a wrong encoding algorithm" in {
      val wrong_algorithm: JwtAlgorithm = JwtAlgorithm.HS224
      val jwt: String = GenerateJWT(username, user_id, role, secretKey, wrong_algorithm, tokenExpiryPeriodInMinutes)
      val JWTToken = Marshal(JwtToken(jwt)).to[MessageEntity].futureValue
      val request = Post("/checkAuth").withEntity(JWTToken)
      request ~> userRoute ~> check {
        response.status shouldEqual StatusCodes.Unauthorized
      }
    }

    "Fail authorization with JWT encoded using null algorithm" in {
      val jwt: String = "{'typ': 'JWT','alg': 'none'}.{'iss': 'Cognira', 'iat': 1554908371, 'exp': 32503676400}."
      val JWTToken = Marshal(JwtToken(jwt)).to[MessageEntity].futureValue
      val request = Post("/checkAuth").withEntity(JWTToken)
      request ~> userRoute ~> check {
        response.status shouldEqual StatusCodes.Unauthorized
      }
    }
  }
}