package com.cognira.routes

import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.{ContentTypes, MessageEntity, StatusCodes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{Matchers, WordSpec}
import com.cognira.models.Tenant

trait TenantRouteSpec extends WordSpec
  with Matchers
  with ScalatestRouteTest
  with TenantMarshaller
  with ScalaFutures {


  private def commonChecks = {
    status shouldBe StatusCodes.OK
    contentType shouldBe ContentTypes.`application/json`
  }

  private val tenantRoute = Route.seal(TenantRoutes.routes)

  "TenantRoutes" should {

    "Add tenant to database" in {
      val tenant_to_add: Tenant = Tenant(1, "tenant2")
      val tenant = Marshal(tenant_to_add).to[MessageEntity].futureValue
      val request = Post("/tenant").withEntity(tenant)

      request ~> tenantRoute ~> check {
        status shouldBe StatusCodes.OK
        responseAs[String] shouldEqual "Tenant added successfully"
      }
    }

    "Update tenant in database" in {
      val tenant_to_update: Tenant = Tenant(1, "new_tenant_name")
      val tenant = Marshal(tenant_to_update).to[MessageEntity].futureValue
      val request = Patch("/tenant/1").withEntity(tenant)

      request ~> tenantRoute ~> check {
        status shouldBe StatusCodes.OK
        responseAs[String] shouldEqual "Tenant updated successfully"
      }
    }

    "Delete tenant in database" in {
      val tenant_to_delete: Tenant = Tenant(1, "new_tenant_name")
      val tenant = Marshal(tenant_to_delete).to[MessageEntity].futureValue
      val request = Delete("/tenant/1").withEntity(tenant)

      request ~> tenantRoute ~> check {
        status shouldBe StatusCodes.OK
        responseAs[String] shouldEqual "Promotion deleted successfully"
      }
    }

    "Get tenant from database" in {
      val request = Get("/tenant/0")

      request ~> tenantRoute ~> check {
        commonChecks
        responseAs[List[Tenant]] should have length 1
      }
    }

  }
}