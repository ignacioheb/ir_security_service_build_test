package com.cognira.models

import com.datastax.driver.core.Row

case class UserLanguage(username: String, language: String)

object UserLanguage {

  def parseEntity(row: Row): UserLanguage = {
    UserLanguage(
      row.getString("username"),
      row.getString("language")
    )
  }

}