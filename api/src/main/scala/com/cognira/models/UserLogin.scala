package com.cognira.models

case class UserLogin(username: String, password: String)