package com.cognira.models

case class Ping(timeIn: String, timeOut: String)
