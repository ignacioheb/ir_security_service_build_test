package com.cognira.models

import com.datastax.driver.core.Row

case class User(tenant_id: Int,
                user_id: Int,
                username: String,
                role: String,
                email: String,
                password: String,
                language: String)

object User {

  /**
    * Create User from cassandra resultset row
    * @param row
    * @return User
    */
  def parseEntity(row: Row): User = {
    User(
      row.getInt("tenant_id"),
      row.getInt("user_id"),
      row.getString("username"),
      row.getString("role"),
      row.getString("email"),
      row.getString("user_password"),
      row.getString("language")
    )
  }

}