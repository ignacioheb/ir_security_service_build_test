package com.cognira.models

import com.datastax.driver.core.Row

case class Tenant(tenant_id: Int,
                  tenant_name: String)

object Tenant {

  /**
    * Create Tenant from cassandra resultset row
    * @param row
    * @return Tenant
    */
  def parseEntity(row: Row): Tenant = {
    Tenant(
      row.getInt("tenant_id"),
      row.getString("tenant_name")
    )
  }

}