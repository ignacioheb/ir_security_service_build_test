package com.cognira.models

case class JwtToken(token: String)