package com.cognira.persistence

import com.datastax.driver.core._
import com.typesafe.config.ConfigFactory

import com.cognira.utils.Config
import com.cognira.utils.Other._

/**
  * Create cassandra session
  */
object CassandraSessionProvider extends Config {

  var test = ConfigFactory.load().getString("test")

  implicit val cassandraConn: Session = {
    val cluster_builder = new Cluster.Builder().withClusterName("Promo Management")
    var cluster: Cluster = null
    if (test == "true"){
      cluster = cluster_builder.addContactPoint("localhost")
        .withPort(9042)
        .build()
    } else {
      val sslOptions = {
        if (MODE == "prod") {
          getSSLOptions(DB_PASS, SSL_CERTFILE, SSL_VERSION)
        } else {
          null
        }
      }
      cluster = cluster_builder.addContactPoint(DB_HOST)
        .withPort(DB_PORT.toInt)
        .withCredentials(DB_USER, DB_PASS)
        .withSSL(sslOptions)
        .build()
    }

    cluster.connect("security_service")
  }
}
