package com.cognira.routes

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server.Directives._
import spray.json.DefaultJsonProtocol
import akka.http.scaladsl.server._
import com.cognira.models.Ping
import com.cognira.services.PingService._


trait PingMarshaller extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val pingFormat = jsonFormat2(Ping.apply)
}

object PingRoute extends PingMarshaller {

  def route: Route = post {
    path("ping") {
      entity(as[Ping]) {
        ping => {
          val pingOut = Ping(ping.timeIn, getCurrentTimeStr)
          complete(pingOut)
        }
      }
    }
  }

}
