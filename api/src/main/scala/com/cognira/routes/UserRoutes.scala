package com.cognira.routes

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.headers.{HttpCookie, RawHeader}
import akka.http.scaladsl.server.Directives._
import spray.json.DefaultJsonProtocol
import akka.http.scaladsl.model.StatusCodes

import scala.concurrent.Future
import scala.util.{Failure, Success}
import monix.execution.Scheduler.Implicits.global
import com.cognira.models.{JwtToken, User, UserLanguage, UserLogin}
import com.cognira.services.UserService._
import com.cognira.utils.JWTutils._
import com.cognira.utils.AsyncHTTP._

trait LoginMarshaller extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val loginFormat = jsonFormat2(UserLogin.apply)
  implicit val jwtFormat = jsonFormat1(JwtToken)
  implicit val userFormat = jsonFormat7(User.apply)
  implicit val userLanguageFormat = jsonFormat2(UserLanguage.apply)
}

object UserRoutes extends LoginMarshaller{

  /**
    * Generate JWT if user exists otherwise reject
    * @param user_login
    * @return Future[Option[String]]
    */
  def userHandler(user_login: UserLogin): Future[Option[String]] = {
    for {
      verify <- verifyCredentials(user_login.username, user_login.password)
      role <- if (verify) getUserRole(user_login.username) else Future("Role Not found")
      user_id <- if (verify) getUserId(user_login.username) else Future("User_id Not found")
    } yield {
      if (verify) Some(GenerateJWT(user_login.username, user_id, role, secretKey, algorithm, tokenExpiryPeriodInMinutes)) else None
    }
  }

  /**
    * Implement the /login and /checkAuth endpoints
    * @return Route
    */
  def postRequest = post {
    path("login") {
      entity(as[UserLogin]){
        entity => {
          onComplete(userHandler(entity)) {
            case Success(value) => value match {
              case None => respondWithHeader(RawHeader("Access-Token", "")) {
                complete(StatusCodes.Unauthorized)
              }
              case Some(s) => {
                setCookie(HttpCookie("Authorization",s)) {
                  complete(getLanguage(entity.username))
                }
              }
            }
            case Failure(e) => complete((StatusCodes.InternalServerError, s"An error occurred: ${e.getMessage}"))
          }
        }
      }
    } ~
    path("checkAuth"){
      entity(as[JwtToken]){
        entity => onComplete(isAuthorized(entity.token)) {
          case Success(value) => value match {
            case true => {
              decode(entity.token, secretKey, algorithm) match {
                case Success(claim) => {
                  complete(claim.content)
                }
                case _ => complete(StatusCodes.Unauthorized, "Unauthorized user")
              }
            }
            case false => complete(StatusCodes.Unauthorized)
          }
          case Failure(e) => complete((StatusCodes.InternalServerError, s"An error occurred: ${e.getMessage}"))
        }
      }
    }
  }

  def AddUserRoute = post {
    path("user") {
      entity(as[User]) {
        user => {
          asyncComplete(insertUser(user))
        }
      }
    }
  }

  def UseridRoutes = path("user"/IntNumber) { id =>
      get {
        asyncComplete(getUser(id))
      } ~
      patch {
        entity(as[User]) { user =>
          asyncComplete(updateUser(id, user))
        }
      } ~
      delete {
        entity(as[User]) { user =>
          asyncComplete(deleteUser(id, user))
        }
      }
    }

  val routes = postRequest ~ AddUserRoute ~ UseridRoutes
}
