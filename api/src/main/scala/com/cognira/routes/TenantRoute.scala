package com.cognira.routes

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Directives._
import spray.json.DefaultJsonProtocol
import akka.http.scaladsl.model.StatusCodes
import scala.concurrent.Future
import scala.util.{Try, Success, Failure}
import scala.concurrent.ExecutionContext.Implicits.global
import com.datastax.driver.core._

import com.cognira.models.Tenant
import com.cognira.services.TenantService._
import com.cognira.utils.AsyncHTTP._

trait TenantMarshaller extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val tenantFormat = jsonFormat2(Tenant.apply)
}

object TenantRoutes extends TenantMarshaller{

  
  def AddTenantRoute = post {
    path("tenant") {
      entity(as[Tenant]) {
        tenant => {
          asyncComplete(insertTenant(tenant))
        }
      }
    }
  }

  def TenantidRoutes = path("tenant"/IntNumber) { id =>
      get {
        asyncComplete(getTenant(id))
      } ~
      patch {
        entity(as[Tenant]) { tenant =>
          asyncComplete(updateTenant(id, tenant))
        }
      } ~
      delete {
        entity(as[Tenant]) { tenant =>
          asyncComplete(deleteTenant(id, tenant))
        }
      }
    }


  val routes = AddTenantRoute ~ TenantidRoutes
}
