package com.cognira.utils

/**
  * Config class to use for connecting to Cassandra database
  * Config is populated from file $(security_service_root_folder)/config/env.conf
  */
class Config {

  var MODE = sys.env.get("MODE").getOrElse("")
  var DB_HOST = sys.env.get("DB_HOST").getOrElse("")
  var DB_PORT = sys.env.get("DB_PORT").getOrElse("")
  var DB_USER = sys.env.get("DB_USER").getOrElse("")
  var DB_PASS = sys.env.get("DB_PASS").getOrElse("")
  var SSL_VERSION = sys.env.get("SSL_VERSION").getOrElse("").replace("_",".") //Scala version is TLSv1.2 not TLSv1_2
  var SSL_VALIDATE = sys.env.get("SSL_VALIDATE").getOrElse("")
  var SSL_CERTFILE = sys.env.get("SSL_CERTFILE").getOrElse("")

}