package com.cognira.utils

import java.time.Instant
import java.util.concurrent.TimeUnit

import pdi.jwt.{JwtAlgorithm, JwtCirce, JwtClaim, algorithms}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

final case class MalformedAuthorization(message: String)
  extends Exception(message) {}

final case class MalformedClaimInAuthorization(message: String)
  extends Exception(message) {}

object JWTutils {

  val tokenExpiryPeriodInMinutes = 180
  val iss = "Cognira"
  val secretKey = "super_secret_key"
  val algorithm = JwtAlgorithm.HS256

  /**
    * Generate JWT for user
    * @param username
    * @param user_id
    * @param role
    * @param secret
    * @param algorithm
    * @param expiry
    * @return String
    */
  def GenerateJWT(username: String, user_id: String, role: String, secret: String, algorithm: JwtAlgorithm, expiry: Long):  String = {
    val claim = JwtClaim(
      issuer = Some(iss),
      content = s"""{"role":"$role", "user_id":"$user_id"}""",
      subject = Some(username),
      issuedAt = Some(Instant.now.getEpochSecond),
      expiration = Some(Instant.now.getEpochSecond + TimeUnit.MINUTES.toSeconds(expiry))
    )
    JwtCirce.encode(claim, secret, algorithm)
  }

  /**
    * Check if JWT is valid
    * @param token
    * @return
    */
  def isValid(token: String, secret: String, algorithm: algorithms.JwtHmacAlgorithm): Boolean = {
    JwtCirce.isValid(token, secret, Seq(algorithm))
  }

  /**
    * Extract JwtClaim from JWT if possible
    * @param token
    * @return Try[JwtClaim]
    */
  def decode(token: String, secret: String, algorithm: algorithms.JwtHmacAlgorithm): Try[JwtClaim] = {
    JwtCirce.decode(token, secret, Seq(algorithm))
  }

  /**
    * Extract User from JWT claims
    * @param token
    * @return Try[String]
    */
  def getUserFromAuth(token: String): Try[String] = {
    decode(token, secretKey, algorithm) match {
      case Success(claim) =>
        claim.subject match {
          case None => Failure(new MalformedClaimInAuthorization("No subject specified in the JWT"))
          case Some(sub) => Success(sub)
        }
      case Failure(e) => Failure(MalformedClaimInAuthorization(e.toString))
    }
  }

  /**
    * Check whether a JWT grants authorization to its bearer or not
    * @param token
    * @return Future[Boolean]
    */
  def isAuthorized(token: String): Future[Boolean] = Future{
    if (isValid(token, secretKey, algorithm)) {
      decode(token, secretKey, algorithm) match {
        case Success(claim) => {
          claim.expiration match {
            case Some(exp) => exp > Instant.now.getEpochSecond
            case None =>
              Failure(new MalformedClaimInAuthorization("No expiration specified in the JWT"))
              false
          }
        }
        case Failure(e) => println(s"Authorization passes isValid but failed to decode: ${e.printStackTrace}")
          false
      }
    } else {
      Failure(new MalformedAuthorization("Authorization is invalid"))
      false
    }
  }
}
