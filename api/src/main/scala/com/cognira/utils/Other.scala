package com.cognira.utils

import java.io.FileReader
import org.bouncycastle.openssl.PEMParser
import com.datastax.driver.core.JdkSSLOptions
import javax.net.ssl.{KeyManagerFactory, SSLContext, TrustManagerFactory}
import java.security._
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter
import java.security.cert.X509Certificate
import org.bouncycastle.cert.X509CertificateHolder
import org.bouncycastle.jce.provider.BouncyCastleProvider

object Other {

  /**
    * Read PEM (certificate) file from filePath
    * @param filePath
    * @return AnyRef
    */
  def readPEMFile(filePath: String) = {
    val reader = new PEMParser(new FileReader(filePath))
    val fileHolder = reader.readObject()
    reader.close()
    fileHolder
  }

  /**
    * Return SSL options object to use to connect to Cassandra
    * @param password
    * @param certfile
    * @param ssl_version
    * @return JdkSSLOptions
    */
  def getSSLOptions(password: String, certfile: String, ssl_version: String): JdkSSLOptions = {
    Security.addProvider(new BouncyCastleProvider())

    val certificateConverter = new JcaX509CertificateConverter().setProvider("BC")

    val keyStore = KeyStore.getInstance(KeyStore.getDefaultType)
    keyStore.load(null, null)

    val certHolder: X509Certificate = certificateConverter.getCertificate(readPEMFile(certfile).asInstanceOf[X509CertificateHolder])
    keyStore.setCertificateEntry("cert", certHolder)

    val kmf: KeyManagerFactory  = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm)
    kmf.init(keyStore, password.toCharArray)

    val tmf: TrustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm)
    tmf.init(keyStore)

    val sc: SSLContext = SSLContext.getInstance(ssl_version)
    sc.init(kmf.getKeyManagers, tmf.getTrustManagers, null)

    val sslOptions: JdkSSLOptions = JdkSSLOptions.builder()
      .withSSLContext(sc)
      .build()

    sslOptions
  }

}