package com.cognira.utils

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import scala.concurrent.Future
import akka.http.scaladsl.marshalling.ToResponseMarshaller
import scala.util.{Try, Success, Failure}
import akka.http.scaladsl.model.StatusCodes._


object AsyncHTTP {
  /**
    * Asynchronously complete the HTTP request
    * @param f
    * @param m
    * @tparam T
    * @return RequestContext => Future[RouteResult]
    */
  def asyncComplete[T](f: Future[T])(implicit m: ToResponseMarshaller[T]): RequestContext => Future[RouteResult] = {
    onComplete(f){
      case Success(result) => complete(result)
      case Failure(e) => complete((InternalServerError,s"Something went wrong: ${e.getMessage}"))
    }
  }
}