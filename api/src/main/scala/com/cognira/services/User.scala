package com.cognira.services

import com.datastax.driver.core._
import monix.execution.Scheduler.Implicits.global
import monix.reactive.Observable
import scala.concurrent.Future
import scala.collection.JavaConverters._

import com.cognira.models.{User, UserLanguage}
import com.cognira.utils.AsyncCQL._

object UserService {


  /**
    * GET userRole by username asynchronously
    * @param username
    * @return Future[String]
    */
  def getUserRole(username: String): Future[String] = {
    val obsRows: Observable[Row] = query(cql"SELECT * FROM user")
    val filtered_user = obsRows.filter( row =>
      row.getString("username") == username)
    val userList: Future[List[User]] = futureList(filtered_user, User.parseEntity)
    userList.map(_.head.role)
  }

  /**
    * GET user_id by username asynchronously
    * @param username
    * @return Future[Int]
    */
  def getUserId(username: String): Future[String] = {
    val obsRows: Observable[Row] = query(cql"SELECT * FROM user")
    val filtered_user = obsRows.filter( row =>
      row.getString("username") == username)
    val userList: Future[List[User]] = futureList(filtered_user, User.parseEntity)
    userList.map(_.head.user_id.toString)
  }

  /**
    * Verify that the username/password combination exists in the database
    * @param username
    * @param password
    * @return Future[Boolean]
    */
  def verifyCredentials(username: String, password: String): Future[Boolean] = Future {
    val query = s"SELECT * FROM user"
    val user_rows = cassandraSession.execute(query)
    val filtered_users = user_rows
                           .asScala
                           .map(row => (row.getString("username"), row.getString("user_password")))
                           .filter(t => t._1 == username && t._2 == password)

    filtered_users.nonEmpty
  }

  /**
    * Create User asynchronously
    * @param userDetail
    * @return Future[Resultset]
    */

  def insertUser(userDetail: User): Future[String] = {
    val result_set: Future[ResultSet] = execute(
      cql"""
          INSERT INTO user(tenant_id, user_id, username, role, email, user_password, language)
          VALUES (?, ?, ?, ?, ?, ?, ?)
         """,
      userDetail.tenant_id, userDetail.user_id, userDetail.username, userDetail.role, userDetail.email, userDetail.password, userDetail.language
    )
        result_set.map(_ => "User added successfully")
  }

  /**
    * Update User asynchronously
    * @param userDetail
    * @return Future[ResultSet]
    */
  def updateUser(id: Int, userDetail: User): Future[String] = {
    if (id != userDetail.user_id){
      throw new IllegalAccessException("User identifier mismatch! You can't update user_id")
    }
    val result_set: Future[ResultSet] = execute(
      cql"""
         UPDATE user SET  username = ?,
                          role = ?,
                          email = ?,
                          user_password = ?,
                          language = ?
         WHERE tenant_id = ?
         AND user_id = ?
         """,
       userDetail.username, userDetail.role, userDetail.email,
       userDetail.password, userDetail.language, userDetail.tenant_id, userDetail.user_id
    )
    result_set.map(_ => "User updated successfully")
  }

  /**
    * Delete user asynchronously
    * @param user_id
    * @return Future[ResultSet]
    */
  def deleteUser(user_id: Int, userDetail: User): Future[String] = {
    if (user_id != userDetail.user_id){
      throw new IllegalAccessException("User identifier mismatch! You can't delete user_id")
    }
    val result_set: Future[ResultSet] = execute(
      cql"DELETE FROM user WHERE tenant_id = ? AND user_id = ?",
      userDetail.tenant_id,
      userDetail.user_id
    )
    result_set.map(_ => "User deleted successfully")
  }

  /**
    * GET user by user_id asynchronously
    * @param user_id
    * @return Future[List[User]]
    */
  def getUser(user_id: Int): Future[List[User]] = {
    val obsRows: Observable[Row] = query(
      cql"SELECT * FROM user"
    )

    val filtered_user = obsRows.filter( row =>
      row.getInt("user_id") == user_id)

    futureList(filtered_user, User.parseEntity)
  }

  /**
    * GET language by username
    * @param username
    * @return Future[List[UserLanguage]]
    */
  def getLanguage(username: String): Future[List[UserLanguage]] = {
    val obsRows: Observable[Row] = query(
      cql"SELECT username, language FROM user"
    )
    val filtered_user = obsRows.filter( row =>
      row.getString("username") == username)
    futureList(filtered_user, UserLanguage.parseEntity)
  }
}
