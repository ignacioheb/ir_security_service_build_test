package com.cognira.services

import java.text.SimpleDateFormat
import java.util.Calendar

object PingService {

  def getCurrentTimeStr: String = {
    val now = Calendar.getInstance.getTime
    val timeformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    timeformat.format(now)
  }

}
