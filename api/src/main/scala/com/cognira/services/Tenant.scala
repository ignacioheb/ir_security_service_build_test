package com.cognira.services

import com.datastax.driver.core._
import monix.execution.Scheduler.Implicits.global
import monix.reactive.Observable
import scala.concurrent.Future
import scala.concurrent.ExecutionContext._

import com.cognira.persistence.CassandraSessionProvider
import com.cognira.models.Tenant
import com.cognira.utils.AsyncCQL._

object TenantService {

  /**
    * Create Tenant asynchronously
    * @param tenantDetail
    * @return Future[Resultset]
    */

  def insertTenant(tenantDetail: Tenant): Future[String] = {
    val result_set: Future[ResultSet] = execute(
      cql"""
          INSERT INTO tenant(tenant_id, tenant_name)
          VALUES (?, ?)
         """,
      tenantDetail.tenant_id, tenantDetail.tenant_name
    )
        result_set.map(_ => "Tenant added successfully")
  }

  /**
    * Update Tenant asynchronously
    * @param tenantDetail
    * @return Future[ResultSet]
    */
  def updateTenant(id: Int, tenantDetail: Tenant): Future[String] = {
    if (id != tenantDetail.tenant_id){
      throw new IllegalAccessException("Tenant identifier mismatch! You can't update tenant_id")
    }
    val result_set: Future[ResultSet] = execute(
      cql"""
         UPDATE tenant SET tenant_name = ?
         WHERE tenant_id = ?
         """,
       tenantDetail.tenant_name, tenantDetail.tenant_id
    )
    result_set.map(_ => "Tenant updated successfully")
  }

  /**
    * Delete tenant asynchronously
    * @param tenant_id
    * @return Future[ResultSet]
    */
  def deleteTenant(tenant_id: Int, tenantDetail: Tenant): Future[String] = {
    if (tenant_id != tenantDetail.tenant_id){
      throw new IllegalAccessException("Tenant identifier mismatch! You can't delete tenant_id")
    }
    val result_set: Future[ResultSet] = execute(
      cql"DELETE FROM tenant WHERE tenant_id = ?",
      tenantDetail.tenant_id
    )
    result_set.map(_ => "Promotion deleted successfully")
  }

  /**
    * GET tenant by tenant_id asynchronously
    * @param tenant_id
    * @return Future[List[Tenant]]
    */
  def getTenant(tenant_id: Int): Future[List[Tenant]] = {
    val obsRows: Observable[Row] = query(
      cql"SELECT * FROM tenant WHERE tenant_id = ? ALLOW FILTERING",
      tenant_id
    )
    futureList(obsRows, Tenant.parseEntity)
  }

}
