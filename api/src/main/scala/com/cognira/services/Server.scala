package com.cognira.services

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import scala.util.{Failure, Success}
import akka.http.scaladsl.server.Directives._

import com.cognira.routes._

object Server extends App {
  val host = "0.0.0.0"
  val port = 9001

  implicit val system: ActorSystem = ActorSystem("security-service")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  import system.dispatcher

  val routes = TenantRoutes.routes ~ UserRoutes.routes ~ PingRoute.route

  val binding = Http().bindAndHandle(routes, host, port)
  binding.onComplete {
    case Success(_) => println(s"Server listening at $host:$port")
    case Failure(error) => println(s"Failed: ${error.getMessage}")
  }
}
