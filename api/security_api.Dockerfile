FROM openjdk:8u181

# Install cassandra-driver
RUN apt-get update && apt-get install -y python-pip --fix-missing
RUN pip install cassandra-driver

# Install cqlsh
RUN \
  wget https://files.pythonhosted.org/packages/10/6e/a98135372c9b51e396f6f3d94f8d45027c888ce5f23ee269d2dd577af292/cqlsh-5.0.4.tar.gz && \
  tar xvf cqlsh-5.0.4.tar.gz && \
  mv cqlsh-5.0.4/* /opt/ && \
  chmod 777 /opt/cqlsh

ADD target/ /opt/target