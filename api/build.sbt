name := "security-service"

organization := "com.cognira"
version := "1.1"

enablePlugins(PackPlugin)

scalaVersion := "2.12.7"

val akkaActorsVersion = "2.5.20"
val akkaStreamsVersion = "2.5.20"
val akkaHttpVersion = "10.1.7"

//Akka dependencies
libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaActorsVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaActorsVersion % Test,
  "com.typesafe.akka" %% "akka-stream" % akkaStreamsVersion,
  "com.typesafe.akka" %% "akka-stream-testkit" % akkaStreamsVersion % Test,
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test,
  "com.datastax.cassandra" % "cassandra-driver-core" % "3.3.0",
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
  "io.spray" %%  "spray-json" % "1.3.5",
  "de.heikoseeberger" %% "akka-http-circe" % "1.23.0",
  "io.circe"          %% "circe-generic" % "0.10.0",
  "com.pauldijou" %% "jwt-core" % "0.13.0",
  "com.pauldijou" %% "jwt-circe" % "0.13.0",
  "org.slf4j" % "slf4j-simple" % "1.6.4"
)

//ScalaTest dependencies
libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.0.5" % "test",
  "org.cassandraunit" % "cassandra-unit" % "3.1.1.0" % "test"
)

// https://mvnrepository.com/artifact/io.monix/monix
libraryDependencies += "io.monix" %% "monix" % "3.0.0-8084549"

libraryDependencies += "org.bouncycastle" % "bcpkix-jdk15on" % "1.48"