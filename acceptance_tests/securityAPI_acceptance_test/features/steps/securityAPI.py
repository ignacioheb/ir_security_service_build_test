from behave import *
from cassandra.cluster import Cluster
from hamcrest import assert_that, equal_to, is_not, has_items
import json
import requests

headers = {'Content-Type':'application/json'}

ping_payload = {
        'timeIn': '',
        'timeOut': ''
}

login_payload = {
        'username': 'user1',
        'password': '00000'
}


login_row = (0, 5, 'user1@cognira.com', 'User', '00000','user1', 'language')
ping_route = 'http://security-api:9001/ping'
login_route = 'http://security-api:9001/login'

@given('a user is populated into table user')
def step_impl(context):
  pStatement = ''
  users = []
  strCQL = "INSERT INTO security_service.user (tenant_id, user_id, email, role, user_password, username, language) VALUES (%s, %s, %s, %s, %s, %s, %s)"
  query = context.connection.execute(strCQL, login_row)
  assert_that(query, is_not(equal_to('')))

@given('the server is running')
def step_impl(context):
  with requests.Session() as s:
    p = s.post(ping_route, data = json.dumps(ping_payload), headers = headers)
    assert_that(p.status_code, equal_to(200))
  assert_that(True, equal_to(True))

@when('a POST query is submitted with the right credentials')
def step_impl(context):
  with requests.Session() as s:
    p = s.post(login_route, data = json.dumps(login_payload), headers = headers)
  context.response = p
  assert_that(p.status_code, equal_to(200))


@then('the response should be the user language')
def step_impl(context):
  expected_response = "[{\"language\":\"language\",\"username\":\"user1\"}]"
  assert_that(context.response.content, equal_to(expected_response.encode('utf-8')))



@then('a cookie should be generated')
def step_impl(context):
  cookie = context.response.cookies.get_dict()
  assert_that(cookie, is_not(equal_to({})))
