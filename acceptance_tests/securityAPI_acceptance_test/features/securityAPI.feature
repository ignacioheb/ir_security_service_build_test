Feature: Testing security API routes

Scenario: Checking login post request
  Given connection to cassandraDB is established
  And a user is populated into table user
  And the server is running
  When a POST query is submitted with the right credentials
  Then the response should be the user language
  And a cookie should be generated
