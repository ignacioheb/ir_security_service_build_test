FROM python:3.6-slim-stretch

RUN echo "==> Install behave and related stuff..."   && \
    pip3 install --no-cache-dir \
         behave \
	     pyhamcrest \
	     requests \
	&& \
    echo "==> Install cassandra-driver library..." && \
    pip3 install --no-cache-dir \
         cassandra-driver

COPY schema_manager_acceptance_test/features features
COPY securityAPI_acceptance_test/features features

ENTRYPOINT ["behave"]
