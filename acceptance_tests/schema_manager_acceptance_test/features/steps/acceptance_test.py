from behave import *
from cassandra.cluster import Cluster
from hamcrest import assert_that, equal_to, is_not
import time

table_names = ['user', 'tenant']

@given('connection to cassandraDB is established')
def step_impl(context):
    cluster = Cluster(['cassandra'])
    session = cluster.connect('security_service')
    context.connection = session
    assert_that(context.connection, is_not(equal_to('')))

@when('tables user and tenant exist')
def step_impl(context):
  names = []
  pStatement = ''
  t = 0
  t0 = time.time()
  indicator = False
  while(not(indicator) and t<10):
    my_names = []
    try:
      strCQL = "SELECT table_name FROM system_schema.tables WHERE keyspace_name = 'security_service'"
      pStatement = context.connection.prepare(strCQL)
      rows = context.connection.execute(pStatement)
      for row in rows:
        my_names.append(row.table_name)
      names = my_names
      indicator = all(elem in names for elem in table_names) and all(elem in table_names for elem in names)
      context.indicator = indicator
    except:
      pass
    t1 = time.time()
    t = t1 - t0
    time.sleep(1)

@then('we should get a success response')
def step_impl(context):
  assert_that(context.indicator, equal_to(True))
