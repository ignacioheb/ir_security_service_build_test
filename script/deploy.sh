#!/bin/bash

#---------------------------------------------------------#
# This script deploys the Promo Planning application by   #
# building and starting up its 3 parts (security,         #
# service, and/or UI).                                    #
# You'll need to supply the required paths and flags.     #
#---------------------------------------------------------#

set -e

#This is needed to correctly interpret PATHs passed as arguments to docker-compose
export MSYS_NO_PATHCONV=1

# Colors
default="\033[39m"
green="\033[32m"
red="\033[31m"

# Optional flags
no_security=false
no_promo=false
no_ui=false

# Displays the script help text
usage() {
    echo -e "Usage: deploy.sh --security-path PATH --promo-path PATH --ui-path PATH [--no-security] [--no-promo] [--no-ui]\n"
    echo -e "Deploy (build then serve) the Promo Planning application.\n"
    echo -e "The available options are:"
    echo -e "  --security-path PATH    Sets the path to the Security service codebase"
    echo -e "  --promo-path PATH       Sets the path to the Promo service codebase"
    echo -e "  --ui-path PATH          Sets the path to the UI codebase"
    echo -e "  --no-security           Flag to not deploy the Security service"
    echo -e "  --no-promo              Flag to not deploy the Promo service"
    echo -e "  --no-ui                 Flag to not deploy the UI"
    echo -e "  --help                  Displays the script usage"
}

# Deploys the common assets
deploy_common() {
    cd "$promo_path"
    echo -e "${green}### Creating network... ###${default}"
    ./script/create-promo-network.sh
    echo -e "${green}### Spinning up Cassandra container... ###${default}"
    docker-compose -f cassandra.yml up -d
    echo -e "${green}### Creating cache data volume... ###${default}"
    ./script/create-sbt-data.sh
}

#Run the Schema Manager service for promo planning
deploy_promo_planning_schema_manager() {
    cd "$promo_path" 
    echo -e "${green}### Deploying Promo Planning schema... ###${default}"
    docker-compose -f schema-manager.yml run\
        -v $(pwd)/schema-manager/tables.list:/root/tables.list\
        --rm schema-manager\
        -k promo_planning\
        -tf /root/tables.list -o
}

#Run the Schema Manager service for security service
deploy_security_schema_manager() {
    cd "$security_path" 
    echo -e "${green}### Deploying Security schema... ###${default}"
    docker-compose -f schema-manager.yml run\
        -v $(pwd)/schema-manager/tables.list:/root/tables.list\
        --rm schema-manager\
        -k security_service\
        -tf /root/tables.list -o
}

# Deploys the Security service
deploy_security() {
    cd "$security_path"
    echo -e "${green}### Packaging Security service... ###${default}"
    docker-compose -f api/build.yml run --rm sbt sbt pack
    echo -e "${green}### Building Security service... ###${default}"
    docker-compose up --build -d
}

# Deploys the Promo service
deploy_promo() {
    cd "$promo_path"
    echo -e "${green}### Packaging Promo service... ###${default}"
    docker-compose -f data_layer/build.yml run --rm sbt sbt publishLocal
    docker-compose -f api/build.yml run --rm sbt sbt pack
    echo -e "${green}### Building Promo service... ###${default}"
    docker-compose up --build -d
}

# Deploys the UI
deploy_ui() {
    cd "$ui_path"
    echo -e "${green}### Creating node-modules volume... ###${default}"
    ./create-node-modules.sh
    echo -e "${green}### Building UI... ###${default}"
    docker-compose up --build -d
    echo -e "${green}### Serving UI... ###${default}"
    docker-compose -f nginx.yml up --build -d
}

# Correct for MacOS (Darwin)
if [ "$(uname -s)" = "Darwin" ]; then
  greadlink --help > /dev/null
  if [ $? -ne 0 ]; then
    echo -e "${red}MacOS deployment requires brew installation of gnu coreutils package.${default}"
    exit 1
  fi
  READLINK=greadlink
else
  READLINK=readlink
fi

# Check for help
if [[ $@ == *"--help"* ]]
then
    usage
    exit 0
fi

# Check for required arguments
if [[ $@ != *"--security-path"* ]]
then
    echo -e "${red}Expected a --security-path argument${default}\n"
    usage
    exit 1
fi
if [[ $@ != *"--promo-path"* ]]
then
    echo -e "${red}Expected a --promo-path argument${default}\n"
    usage
    exit 1
fi
if [[ $@ != *"--ui-path"* ]]
then
    echo -e "${red}Expected a --ui-path argument${default}\n"
    usage
    exit 1
fi

# Parse arguments
while [[ $# > 0 ]]
do
    arg="$1"
    shift
    case $arg in
        --security-path)
            if [[ "$1" = "--"* || -z "$1" ]]; then
                echo -e "${red}Expected a value for --security-path${default}\n"
                usage
                exit 1
            fi
            security_path=$(${READLINK} -f "${1%/}")
            shift
        ;;
        --promo-path)
            if [[ "$1" = "--"* || -z "$1" ]]; then
                echo -e "${red}Expected a value for --promo-path${default}\n"
                usage
                exit 1
            fi
            promo_path=$(${READLINK} -f "${1%/}")
            shift
        ;;
        --ui-path)
            if [[ "$1" = "--"* || -z "$1" ]]; then
                echo -e "${red}Expected a value for --ui-path${default}\n"
                usage
                exit 1
            fi
            ui_path=$(${READLINK} -f "${1%/}")
            shift
        ;;
        --no-security)
            no_security=true
        ;;
        --no-promo)
            no_promo=true
        ;;
        --no-ui)
            no_ui=true
        ;;
        *)
            echo -e "${red}'$arg' unexpected${default}\n"
            usage
            exit 1
        ;;
    esac
done

# Check for paths existance
if [[ ! -d "$security_path" ]]
then
    echo -e "${red}Supplied Security service path does not exist${default}"
    exit 1
fi
if [[ ! -d "$promo_path" ]]
then
    echo -e "${red}Supplied Promo service path does not exist${default}"
    exit 1
fi
if [[ ! -d "$ui_path" ]]
then
    echo -e "${red}Supplied UI path does not exist${default}"
    exit 1
fi

# Deploy application
echo -e "${green}### Starting... ###${default}"
deploy_common
if [[ $no_security == false ]]
then
    deploy_security_schema_manager
    deploy_security
fi
if [[ $no_promo == false ]]
then
    deploy_promo_planning_schema_manager
    deploy_promo
fi
if [[ $no_ui == false ]]
then
    deploy_ui
fi
echo -e "${green}### Exiting... ###${default}"
