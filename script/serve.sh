#!/bin/bash

#---------------------------------------------------------#
# This script serves the Promo Planning application by    #
# starting up its 3 parts (security, service, and/or UI). #
# You'll need to supply the required paths and flags.     #
#---------------------------------------------------------#

set -e

#This is needed to correctly interpret PATHs passed as arguments to docker-compose
export MSYS_NO_PATHCONV=1

# Colors
default="\033[39m"
green="\033[32m"
red="\033[31m"

# Optional flags
no_security=false
no_promo=false
no_ui=false

# Displays the script help text
usage() {
    echo -e "Usage: serve.sh --security-path PATH --promo-path PATH --ui-path PATH [--no-security] [--no-promo] [--no-ui]\n"
    echo -e "Serve the Promo Planning application.\n"
    echo -e "The available options are:"
    echo -e "  --security-path PATH    Sets the path to the Security service codebase"
    echo -e "  --promo-path PATH       Sets the path to the Promo service codebase"
    echo -e "  --ui-path PATH          Sets the path to the UI codebase"
    echo -e "  --no-security           Flag to not serve the Security service"
    echo -e "  --no-promo              Flag to not serve the Promo service"
    echo -e "  --no-ui                 Flag to not serve the UI"
    echo -e "  --help                  Displays the script usage"
}

#Run the Schema Manager service for promo planning
deploy_promo_planning_schema_manager() {
    cd "$promo_path" 
    echo -e "${green}### Deploying Promo Planning schema... ###${default}"
    docker-compose -f schema-manager.yml run\
        -v $(pwd)/schema-manager/tables.list:/root/tables.list\
        --rm schema-manager\
        -k promo_planning\
        -tf /root/tables.list -o
}

#Run the Schema Manager service for security service
deploy_security_schema_manager() {
    cd "$security_path" 
    echo -e "${green}### Deploying Security schema... ###${default}"
    docker-compose -f schema-manager.yml run\
        -v $(pwd)/schema-manager/tables.list:/root/tables.list\
        --rm schema-manager\
        -k security_service\
        -tf /root/tables.list -o
}

# Serves the common assets
serve_common() {
    cd "$promo_path"
    echo -e "${green}### Spinning up Cassandra container... ###${default}"
    docker-compose -f cassandra.yml up -d
}

# Serves the Security service
serve_security() {
    cd "$security_path"
    deploy_security_schema_manager
    echo -e "${green}### Building Security service... ###${default}"
    docker-compose up -d
}

# Serves the Promo service
serve_promo() {
    cd "$promo_path"
    deploy_promo_planning_schema_manager
    echo -e "${green}### Building Promo service... ###${default}"
    docker-compose up -d
}

# Serves the UI
serve_ui() {
    cd "$ui_path"
    echo -e "${green}### Serving UI... ###${default}"
    docker-compose -f nginx.yml up -d
}

# Check for help
if [[ $@ == *"--help"* ]]
then
    usage
    exit 0
fi

# Check for required arguments
if [[ $@ != *"--security-path"* ]]
then
    echo -e "${red}Expected a --security-path argument${default}\n"
    usage
    exit 1
fi
if [[ $@ != *"--promo-path"* ]]
then
    echo -e "${red}Expected a --promo-path argument${default}\n"
    usage
    exit 1
fi
if [[ $@ != *"--ui-path"* ]]
then
    echo -e "${red}Expected a --ui-path argument${default}\n"
    usage
    exit 1
fi

# Correct for MacOS (Darwin)
if [ "$(uname -s)" = "Darwin" ]; then 
  greadlink --help > /dev/null
  if [ $? -ne 0 ]; then
    echo -e "${red}MacOS deployment requires brew installation of gnu coreutils package.${default}"
    exit 1
  fi
  READLINK=greadlink 
else 
  READLINK=readlink
fi

# Parse arguments
while [[ $# > 0 ]]
do
    arg="$1"
    shift
    case $arg in
        --security-path)
            if [[ "$1" = "--"* || -z "$1" ]]; then
                echo -e "${red}Expected a value for --security-path${default}\n"
                usage
                exit 1
            fi
            security_path=$(${READLINK} -f "${1%/}")
            shift
        ;;
        --promo-path)
            if [[ "$1" = "--"* || -z "$1" ]]; then
                echo -e "${red}Expected a value for --promo-path${default}\n"
                usage
                exit 1
            fi
            promo_path=$(${READLINK} -f "${1%/}")
            shift
        ;;
        --ui-path)
            if [[ "$1" = "--"* || -z "$1" ]]; then
                echo -e "${red}Expected a value for --ui-path${default}\n"
                usage
                exit 1
            fi
            ui_path=$(${READLINK} -f "${1%/}")

            shift
        ;;
        --no-security)
            no_security=true
        ;;
        --no-promo)
            no_promo=true
        ;;
        --no-ui)
            no_ui=true
        ;;
        *)
            echo -e "${red}'$arg' unexpected${default}\n"
            usage
            exit 1
        ;;
    esac
done

# Check for paths existance
if [[ ! -d "$security_path" ]]
then
    echo -e "${red}Supplied Security service path does not exist${default}"
    exit 1
fi
if [[ ! -d "$promo_path" ]]
then
    echo -e "${red}Supplied Promo service path does not exist${default}"
    exit 1
fi
if [[ ! -d "$ui_path" ]]
then
    echo -e "${red}Supplied UI path does not exist${default}"
    exit 1
fi

# Deploy application
echo -e "${green}### Starting... ###${default}"
serve_common
if [[ $no_security == false ]]
then
    serve_security
fi
if [[ $no_promo == false ]]
then
    serve_promo
fi
if [[ $no_ui == false ]]
then
    serve_ui
fi
echo -e "${green}### Exiting... ###${default}"
