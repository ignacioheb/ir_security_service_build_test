#!/bin/bash

if [ "$(docker network ls -f name=promo-network -q)" = "" ]; then
  echo "Creating Docker network:"
  docker network create promo-network
  echo "Network created"
else
  echo "Network already exists, nothing to do."
fi
