#!/bin/bash

#---------------------------------------------------------#
# This script builds the Promo Planning application by    #
# building its 3 parts (security, service, and/or UI).    #
# You'll need to supply the required paths and flags.     #
#---------------------------------------------------------#

set -e

# Colors
default="\e[39m"
green="\e[32m"
red="\e[31m"

# Optional flags
no_security=false
no_promo=false
no_ui=false

# Displays the script help text
usage() {
    echo -e "Usage: build.sh --security-path PATH --promo-path PATH --ui-path PATH [--no-security] [--no-promo] [--no-ui]\n"
    echo -e "Build the Promo Planning application.\n"
    echo -e "The available options are:"
    echo -e "  --security-path PATH    Sets the path to the Security service codebase"
    echo -e "  --promo-path PATH       Sets the path to the Promo service codebase"
    echo -e "  --ui-path PATH          Sets the path to the UI codebase"
    echo -e "  --no-security           Flag to not build the Security service"
    echo -e "  --no-promo              Flag to not build the Promo service"
    echo -e "  --no-ui                 Flag to not build the UI"
    echo -e "  --help                  Displays the script usage"
}

# Builds the common assets
build_common() {
    cd "$promo_path"
    echo -e "${green}### Creating network... ###${default}"
    ./script/create-promo-network.sh
    echo -e "${green}### Creating cache data volume... ###${default}"
    ./script/create-sbt-data.sh
}

# Builds the Security service
build_security() {
    cd "$security_path"
    echo -e "${green}### Packaging Security service... ###${default}"
    docker-compose -f api/build.yml run --rm sbt sbt pack
    echo -e "${green}### Building Security service... ###${default}"
    docker-compose build
}

# Builds the Promo service
build_promo() {
    cd "$promo_path"
    echo -e "${green}### Packaging Promo service... ###${default}"
    docker-compose -f api/build.yml run --rm sbt sbt pack
    echo -e "${green}### Building Promo service... ###${default}"
    docker-compose build
}

# Builds the UI
build_ui() {
    cd "$ui_path"
    echo -e "${green}### Creating node-modules volume... ###${default}"
    ./create-node-modules.sh
    echo -e "${green}### Building UI... ###${default}"
    docker-compose up --build -d
    docker-compose -f nginx.yml build
}

# Check for help
if [[ $@ == *"--help"* ]]
then
    usage
    exit 0
fi

# Check for required arguments
if [[ $@ != *"--security-path"* ]]
then
    echo -e "${red}Expected a --security-path argument${default}\n"
    usage
    exit 1
fi
if [[ $@ != *"--promo-path"* ]]
then
    echo -e "${red}Expected a --promo-path argument${default}\n"
    usage
    exit 1
fi
if [[ $@ != *"--ui-path"* ]]
then
    echo -e "${red}Expected a --ui-path argument${default}\n"
    usage
    exit 1
fi

# Parse arguments
while [[ $# > 0 ]]
do
    arg="$1"
    shift
    case $arg in
        --security-path)
            if [[ "$1" = "--"* || -z "$1" ]]; then
                echo -e "${red}Expected a value for --security-path${default}\n"
                usage
                exit 1
            fi
            security_path=$(readlink -f "${1%/}")
            shift
        ;;
        --promo-path)
            if [[ "$1" = "--"* || -z "$1" ]]; then
                echo -e "${red}Expected a value for --promo-path${default}\n"
                usage
                exit 1
            fi
            promo_path=$(readlink -f "${1%/}")
            shift
        ;;
        --ui-path)
            if [[ "$1" = "--"* || -z "$1" ]]; then
                echo -e "${red}Expected a value for --ui-path${default}\n"
                usage
                exit 1
            fi
            ui_path=$(readlink -f "${1%/}")
            shift
        ;;
        --no-security)
            no_security=true
        ;;
        --no-promo)
            no_promo=true
        ;;
        --no-ui)
            no_ui=true
        ;;
        *)
            echo -e "${red}'$arg' unexpected${default}\n"
            usage
            exit 1
        ;;
    esac
done

# Check for paths existance
if [[ ! -d "$security_path" ]]
then
    echo -e "${red}Supplied Security service path does not exist${default}"
    exit 1
fi
if [[ ! -d "$promo_path" ]]
then
    echo -e "${red}Supplied Promo service path does not exist${default}"
    exit 1
fi
if [[ ! -d "$ui_path" ]]
then
    echo -e "${red}Supplied UI path does not exist${default}"
    exit 1
fi

# Deploy application
echo -e "${green}### Starting... ###${default}"
build_common
if [[ $no_security == false ]]
then
    build_security
fi
if [[ $no_promo == false ]]
then
    build_promo
fi
if [[ $no_ui == false ]]
then
    build_ui
fi
echo -e "${green}### Exiting... ###${default}"
